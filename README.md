# Aircall hiring test


- Application url -> https://cbyr0tyipb.execute-api.us-east-1.amazonaws.com/test/image

- Resized Images
    
    - https://aircall-bucket-francisco.s3.amazonaws.com/aircall.jpg_75    
    - https://aircall-bucket-francisco.s3.amazonaws.com/aircall.jpg_200
    - https://aircall-bucket-francisco.s3.amazonaws.com/aircall.jpg

- Gitlab repo -> https://gitlab.com/lastkidinworld/aircall


## Getting started

Tecnologies that I used to this test

    Terraform
        Infra as code
    AWS
        API Gateway
        Lambda
        S3 bucket   
        CloudWatch
    GitLab
        CICD deploy to S3

Two images were add to his repo to show the flow

Problems:
    Some problems were faced during this test, but I learned so much and I enjoyed

## Authors 
Francisco Palma


